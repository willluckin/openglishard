import pygame
import numpy as np
from pygame.locals import *
from OpenGL import *
from OpenGL.GL import *
from OpenGL.GLU import *

# initalise everything with pygame, creating a context et al
pygame.init()
clock = pygame.time.Clock()
pygame.display.set_caption("will.luckin.co.uk")
display = (800, 600)
screen = pygame.display.set_mode(display, pygame.DOUBLEBUF | pygame.OPENGL)
done = False    # are we (not) running the main loop?

vertices = np.array([0.0, 0.5,
                     0.5, -0.5,
                     -0.5, -0.5], dtype=np.float32)

# create a Vertex Array Object to store all of the information about the attributes and the vbo
# (the posAttrib stuff a while below)
vao = GLuint()
glGenVertexArrays(1, vao)
glBindVertexArray(vao)

# create a buffer to draw to
vbo = GLuint()     # create an uint, value 0, as the index for the buffer
glGenBuffers(1, vbo)    # create one buffer and store it at the index vbo (I think)
glBindBuffer(GL_ARRAY_BUFFER, vbo)      # bind this buffer as the one we are currently drawing to
# draw to the buffer as GL_STATIC_DRAW: Uploaded once, drawn a lot
glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)
vertexShader = glCreateShader(GL_VERTEX_SHADER)    # returns a GLuint representing the shader

with open("./vertshader.glsl") as f:
    shader = f.read()

# source the shader from the shader file
glShaderSource(vertexShader, shader)
# compile the shader on the GPU
glCompileShader(vertexShader)
# did it complete without errors??!

print glGetShaderInfoLog(vertexShader)

with open("./fragshader.glsl") as f:
    shader = f.read()

fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
glShaderSource(fragmentShader, shader)
glCompileShader(fragmentShader)

print glGetShaderInfoLog(fragmentShader)

# now combine our two shaders into a program
shaderProgram = glCreateProgram()
# we need to let the fragment shader know which buffer to write to
# if there's an error it's probably in this line, the tutorial says to use a zero but I think
# it's just being specific to their vbo rather than using the generic variable
# glBindFragDataLocation(shaderProgram, vbo, "outColor")
# attach the shaders to the program
for i in [vertexShader, fragmentShader]:
    glAttachShader(shaderProgram, i)
# now we link the program (sending it to the gpu? probably?)
glLinkProgram(shaderProgram)
# only one program can be active at a time
glUseProgram(shaderProgram)

# where in the inputs to the shader does the position come
posAttrib = glGetAttribLocation(shaderProgram, "position")
# what is the data for this input organised like?
glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0)
# "enable the vertex attribute array" - if this isn't done, the attribute is ignored during rendering
glEnableVertexAttribArray(posAttrib)

print glGetError()

while not done:
    dt = clock.tick(10)
    glDrawArrays(GL_TRIANGLES, 0, 3)
    pygame.display.flip()
    for event in pygame.event.get():  # User did something
        if event.type == pygame.QUIT:  # If user clicked close
            done = True  # Flag that we are done so we exit this loop

pygame.quit()